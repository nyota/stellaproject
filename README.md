# README #

Website name: localhost/myproject/

NOTE: Below commands are important to run in order to install dependecies
1. npm install bootstrap
2. npm install jquery
3. run git command to clone my website: git clone (link of mywebsite)
4. CSS are inside css folder

* html, html5, css, css3  

There 4 links in the site.
1. Home page name is: index.html
    Used viewport, bootstrap, and media querly in different mode of portrait and 
    landscape

    CSS filename is: style.css

2. Landing page name: registrationForm.html
    Used viewport and media query in different mode of portrait and 
    landscape

     CSS filename is: style_register.css


# MOBILE FIRST #
3. Login Page is: login.html
    .col-xs- for Extra Small	Phones Less than 768px to
    .col-sm-	>>>>> Small Devices	Tablets 768px and Up (mobile first)
    .col-md-	>>>>> Medium Devices	Desktops 992px and Up

4. Contact page name: contact.html
    Used viewport and Media Query in different mode of portrait and 
    landscape

# MEDIAQUERY #
/* 
   ##Device = Tablets, Ipads (portrait)
*/

@media (min-width: 768px) and (max-width: 1024px)  and (orientation: portrait) {   
}

/* 
  ##Device = Tablets, Ipads (landscape)
*/

@media (min-width: 768px) and (max-width: 1024px) and (orientation: landscape) { 
}


/* 
##Device =  Most of the Smartphones Mobiles (Portrait, Landscape) 
*/

@media (min-width: 320px) and (max-width: 480px) and (orientation: portrait) {   
}


/* 
  ##Device =  Mobiles (Portrait, Landscape) 
*/
@media (min-width: 481px) and (max-width: 767px)  and (orientation: landscape){
  
}
}